package modele;

import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import infrastructure.jaxrs.HyperLien;
import infrastructure.jaxrs.HyperLiens;
import infrastructure.jaxrs.annotations.ReponsesPOSTCreated;
import infrastructure.jaxrs.annotations.ReponsesPUTOption;

import static configuration.JAXRS.*;

public interface Bibliotheque {
	@PUT
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	@ReponsesPUTOption
	Optional<HyperLien<Livre>> chercher(Livre l);
	
	@POST
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	@Path(SOUSCHEMIN_CATALOGUE)
	@ReponsesPOSTCreated
	HyperLiens<Livre> repertorier();
	
}
