package modele;

public interface IdentifiantLivre {
	String getId();	
	public static IdentifiantLivre fromString(String t) {
		return new ImplemIdentifiantLivre(t);
	}
}
