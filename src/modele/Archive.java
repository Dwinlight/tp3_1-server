package modele;

import static configuration.JAXRS.SOUSCHEMIN_TITRE;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import infrastructure.jaxrs.HyperLien;
import infrastructure.jaxrs.annotations.ReponsesGETNull404;
import infrastructure.jaxrs.annotations.ReponsesPOSTCreated;

public interface Archive {
	//Pas d'annotation HTTP
	@Path("{id}")
	@ReponsesGETNull404
	Livre sousRessource(@PathParam("id") IdentifiantLivre id);
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_XML)
	@ReponsesGETNull404
	Livre getRepresentation(@PathParam("id") IdentifiantLivre id);
	@POST
	@Produces(MediaType.APPLICATION_XML)
	@Consumes(MediaType.APPLICATION_XML)
	@ReponsesPOSTCreated
	HyperLien<Livre> ajouter(Livre l);
}
